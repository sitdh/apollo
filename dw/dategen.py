import math
from datetime import date, timedelta
from dw.models import DimDate
from airflow.hooks.postgres_hook import PostgresHook

class DateCalculator(object):
    def __init__(self, cal_date: date):
        self._date = cal_date

    @classmethod
    def calculate(cls, caldate: date):
        return cls(cal_date=caldate)

    @property
    def entity(self):
        return {
            'DateKey': self.dateKey,
            'FullDate': self.fullDate,
            'MonthCalendar': self.monthCalendar,
            'YearCalendar': self.yearCalendar,
            'YearCalendarShort': self.yearCalendarShort,
            'DayNumberOfWeek': self.dayNumberOfWeek,
            'DayNameOfWeek': self.dayNameOfWeek,
            'DayNumberOfMonth': self.dayNumberOfMonth,
            'DayNumberOfYear': self.dayNumberOfYear,
            'WeekNumberOfMonth': self.weekNumberOfMonth,
            'WeekNumberNameOfMonth': self.weekNumberNameOfMonth,
            'WeekNumberOfYear': self.weekNumberOfYear,
            'WeekNumberNameOfYear': self.weekNumberNameOfYear,
            'MonthNameOfYear': self.monthNameOfYear,
            'MonthNameShortOfYear': self.monthNameShortOfYear,
            'MonthNumberOfYear': self.monthNumberOfYear,
            'QuarterNumberCalendar': self.quarterNumberCalendar,
            'QuarterNameCalendar': self.quarterNameCalendar,
        }

    @property
    def dateKey(self) -> int:
        return int(self._date.strftime('%Y%m%d'))

    @property
    def fullDate(self) -> date:
        return self._date

    @property
    def monthCalendar(self) -> int:
        return int(self._date.strftime('%m'))

    @property
    def yearCalendar(self) -> int:
        return int(self._date.strftime('%Y'))

    @property
    def yearCalendarShort(self) -> int:
        return int(self._date.strftime('%y'))

    @property
    def dayNumberOfWeek(self) -> int:
        return self._date.isoweekday()

    @property
    def dayNameOfWeek(self) -> str:
        return self._date.strftime('%a')

    @property
    def dayNumberOfMonth(self) -> int:
        return int(self._date.strftime('%d'))

    @property
    def dayNumberOfYear(self) -> int:
        return int(self._date.strftime('%j'))

    @property
    def weekNumberOfMonth(self) -> int:
        return math.ceil((
            self._date.day
            + self._date.replace(day=1).isoweekday()
            + 1
        ) / 7.0)

    @property
    def weekNumberNameOfMonth(self) -> str:
        w = self.weekNumberOfMonth
        return f'w{w}'

    @property
    def weekNumberOfYear(self) -> int:
        return int(self._date.strftime('%V'))

    @property
    def weekNumberNameOfYear(self) -> str:
        w = self.weekNumberOfYear
        return f'w{w}'

    @property
    def monthNameOfYear(self) -> str:
        return self._date.strftime('%B')

    @property
    def monthNameShortOfYear(self) -> str:
        return self._date.strftime('%b')

    @property
    def monthNumberOfYear(self) -> int:
        return int(self._date.strftime('%m'))

    @property
    def quarterNumberCalendar(self) -> int:
        q = int(self._date.strftime('%m'))
        w = 1
        if 4 <= q <= 6:
            w = 2
        elif 7 <= q <= 9:
            w = 3
        elif 10 <= q:
            w = 4

        return w

    @property
    def quarterNameCalendar(self) -> str:
        w = self.quarterNumberCalendar
        return f'q{w}'

def date_calculator(date_range):
    return [DateCalculator.calculate(d).entity for d in date_range]

def insert_dates(entities):
    pg_hook = PostgresHook(
        postgres_conn_id='dw_connection',
        schema='airflow'
    )
    engine = pg_hook.get_sqlalchemy_engine()
    engine.execute(
        DimDate.__table__.insert(),
        entities
    )