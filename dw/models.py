import logging
import datetime
from datetime import datetime, timedelta, date

from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Date, DateTime, Integer, Enum, Float, SmallInteger, BigInteger, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, Session
from sqlalchemy.orm import scoped_session, sessionmaker

from airflow.models import Variable
from airflow.hooks.postgres_hook import PostgresHook

pg_hook = PostgresHook(
    postgres_conn_id="dw_connection",
    schema="airflow"
)

engine = pg_hook.get_sqlalchemy_engine()
session = Session(bind=engine)

Base = declarative_base()
Base.query = scoped_session(sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=engine
))

class DimDate(Base):
    __tablename__ = 'DimDate'

    _dateKey               = Column('DateKey', Integer, primary_key=True)
    fullDate               = Column('FullDate', Date)
    monthCalendar          = Column('MonthCalendar', SmallInteger)
    yearCalendar           = Column('YearCalendar', SmallInteger)
    yearCalendarShort      = Column('YearCalendarShort', SmallInteger)
    dayNumberOfWeek        = Column('DayNumberOfWeek', SmallInteger)
    dayNameOfWeek          = Column('DayNameOfWeek', String(length=15))
    dayNumberOfMonth       = Column('DayNumberOfMonth', SmallInteger)
    dayNumberOfYear        = Column('DayNumberOfYear', SmallInteger)
    weekNumberOfMonth      = Column('WeekNumberOfMonth', SmallInteger)
    weekNumberNameOfMonth  = Column('WeekNumberNameOfMonth', String(length=2))
    weekNumberOfYear       = Column('WeekNumberOfYear', SmallInteger)
    weekNumberNameOfYear   = Column('WeekNumberNameOfYear', String(length=3))
    monthNameOfYear        = Column('MonthNameOfYear', String(10))
    monthNameShortOfYear   = Column('MonthNameShortOfYear', String(3))
    monthNumberOfYear      = Column('MonthNumberOfYear', SmallInteger)
    quarterNumberCalendar  = Column('QuarterNumberCalendar', SmallInteger)
    quarterNameCalendar    = Column('QuarterNameCalendar', String(length=3))

    @property
    def dateKey(self) -> int:
        return self.dateKey

    @dateKey.setter
    def dateKey(self, dateKey: int) -> None:
        self._dateKey = dateKey if dateKey > 19000000 else self._dateKey
        self._dateKey = self._dateKey if self._dateKey else 99990101

class DimRegion(Base):
    __tablename__ = 'DimRegion'

    regionKey = Column('RegionKey', Integer, primary_key=True)
    provinceCode = Column('ProvinceCode', SmallInteger)
    provinceNameTH = Column('ProvinceNameTH', String(length=50))
    provinceNameEN = Column('ProvinceNameEN', String(length=25))
    districtCode = Column('DistrictCode', Integer)
    districtNameTH = Column('DistrictNameTH', String(length=85))
    districtNameEN = Column('DistrictNameEN', String(length=45))
    subdistrictCode = Column('SubdistrictCode', Integer)
    subdistrictNameTH = Column('SubdistrictNameTH', String(length=70))
    subdistrictNameEN = Column('SubdistrictNameEN', String(length=35))

class DimProject(Base):
    __tablename__ = 'DimProject'

    projectKey = Column('ProjectKey', Integer, primary_key=True, autoincrement=True)
    projectAlternateKey = Column('ProjectAlternateKey', Integer)
    projectName = Column('ProjectName', String(length=50))
    regionKey = Column('RegionKey', Integer)
    activeUnits = Column('ActiveUnits', Integer)
    assignee = Column('Assignee', Integer)
    startDate = Column('StartDate', Date, default=date.today())
    endDate = Column('EndDate', Date)
    createdAt = Column(
        'CreatedAt',
        Date,
        default=date.today(),
        nullable=True
    )
    endAt = Column(
        'EndAt',
        Date,
        nullable=True,
        default=datetime(9999, 12, 31).date()
    )
    isCurrent = Column('IsCurrent', Boolean, default=False)

    def pack(self):
        return {
            'ProjectKey': self.projectAlternateKey,
            'ProjectAlternateKey': self.projectAlternateKey,
            'ProjectName': self.projectName,
            'RegionKey': self.regionKey,
            'ActiveUnits': self.activeUnits,
            'Assignee': self.assignee,
            'StartDate': self.startDate,
            'EndDate': self.endDate,
            'CreatedAt': self.createdAt,
            'EndAt': self.endAt,
            'IsCurrent': self.isCurrent,
        }

class DimEmployee(Base):
    __tablename__ = 'DimEmployee'

    employeeKey = Column('EmployeeKey', Integer, primary_key=True, autoincrement=True)
    employeeIdAlternateKey = Column('EmployeeIdAlternateKey', String(length=30))

    startDate = Column('StartDate', Date, default=datetime.utcnow)
    endDate = Column('EndDate', Date, nullable=True)

import enum
class BinaryAnswer(enum.Enum):
    YES = 'Yes'
    NO = 'No'

class FactListing(Base):
    __tablename__ = 'FactListing'

    listingKey = Column('ListingKey', BigInteger, primary_key=True, autoincrement=True)
    unitIdAlternateKey = Column('UnitIdAlternateKey', String(length=10))
    unitStatus = Column('UnitStatus', String(20))
    propertyType = Column('PropertyType', String(length=25))
    bedRooms = Column('BedRooms', SmallInteger, default=-1, nullable=True)
    bathRooms = Column('BathRooms', SmallInteger, default=-1, nullable=True)
    indoorArea = Column('IndorArea', Float(2), default=-1, nullable=True)
    floor = Column('Floor', String(length=5), default="N/A", nullable=True)
    dealType = Column('DealType', String(length=25))
    ownership = Column('Ownership', String(length=30))

    forSale = Column('ForSale', String(length=5), default=BinaryAnswer.NO, nullable=True)
    unitPrice = Column('UnitPrice', Float(precision=2), default=-1, nullable=True)
    salePriceName = Column('SalePriceName', String(length=5), default='N/A', nullable=True)

    forRent = Column('ForRent', String(length=5), default=BinaryAnswer.NO, nullable=True)
    monthlyRentalRate = Column('MonthlyRentalRate', Float(precision=2), default=-1, nullable=True)
    rentalRatePriceName = Column('RentalRatePriceName', String(length=10))

    regionKey = Column('RegionKey', Integer)
    dateKey = Column('DateKey', Integer)

    employeeKey = Column('EmployeeKey', SmallInteger)
    projectKey = Column('ProjectKey', Integer)

    startDate = Column('StartDate', Date, default=datetime.utcnow)
    endDate = Column('EndDate', Date, default=datetime.utcnow, nullable=True)

    previousListingId = Column('PreviousListingId', BigInteger, nullable=True, default=-1)
    isCurrentState = Column('IsCurrentState', Boolean, default=False)

def init_db():
    init_status = False
    try:
        pg_hook = PostgresHook(
            postgres_conn_id="dw_connection",
            schema="airflow"
        )
        engine = pg_hook.get_sqlalchemy_engine()
        Base.metadata.create_all(bind=engine)

        init_status = True
    except:
        pass

    return init_status