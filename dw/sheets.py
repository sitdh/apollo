import logging
import datetime

from airflow.models import Variable
from airflow.providers.google.suite.hooks.sheets import GSheetsHook
from airflow.hooks.postgres_hook import PostgresHook

from dw.models import DimProject, DimEmployee, DimRegion, FactListing
from dw.models import session

sheet = GSheetsHook(gcp_conn_id='dw_gsheet_connections')
regions = None

def get_worksheet_content(sheet_id, data_range):
    return sheet.get_values(
        spreadsheet_id=sheet_id,
        range_=data_range
    )

def get_content_worksheet():
    logging.info('Getting GSheet connection')
    sheet_id = Variable.get('content_sheet')
    query_range = Variable.get('content_sheet_target_range')

    logging.info(f'Getting `Content - BKK` sheet id #{sheet_id}')

    return get_worksheet_content(sheet_id=sheet_id, data_range=query_range)

def get_monthly_target_worksheet():
    logging.info('Getting GSheet connection')
    sheet_id = Variable.get('monthly_target_sheet')
    query_range = Variable.get('monthly_target_sheet_target_range')

    logging.info(f'Getting `Monthly target` sheet id #{sheet_id}')

    return get_worksheet_content(sheet_id=sheet_id, data_range=query_range)

def get_employee():
    logging.info('Getting GSheet connection')

    employees_sheet = get_worksheet_content(
        sheet_id=Variable.get('content_sheet'),
        data_range=Variable.get('content_sheet_units_range')
    )

    return employees_sheet

def get_monthly_target_matterport():
    logging.info('Getting GSheet connection')

    target_matterport_sheet = get_worksheet_content(
        sheet_id=Variable.get('monthly_target_matterport_sheet'),
        data_range=Variable.get('monthly_target_matterport_ongoing_range')
    )

    return target_matterport_sheet

def employee_filter(content_employee=None, content_unit_sheet=None, monthly_main_sheet=None, monthly_matterport=None):
    permit_symbols = ('-', '')

    employee = [e[0] for e in content_employee] \
        + [e[2] for e in content_unit_sheet] \
        + [e[14] for e in content_unit_sheet] \
        + [e[17] for e in content_unit_sheet] \
        + [e[1] for e in monthly_main_sheet] \
        + [e[2] for e in monthly_matterport if len(e) > 3 and e[2]]

    return set([e.strip() for e in employee if e not in permit_symbols])

def get_all_regions():
    global regions

    if None == regions:
        regions = session.query(DimRegion).all()

    return regions

def project_filter(content_rows=None, monthly_target_rows=None):
    projects = {}
    regions = get_all_regions()

    projectIds = []
    for row in monthly_target_rows:
        if '-' == row[25] or row[0] in ('1000-01-01', '', None) or '-' == row[27]:
            continue

        projectId = row[25]
        if not projectId in projects:
            pid = f'P{projectId}'
            projectIds.append(pid)
            projects[pid] = {
                'ProjectAlternateKey': projectId,
                'ProjectName': row[4],
                'Priority': 'High',
                'ProjectStatus': row[27],
                'Assignee': row[1],
                'StartDate': datetime.datetime.strptime(row[0], '%Y-%m-%d'),
                'EndDate': datetime.datetime(9999, 12, 31).date(), 
            }

    content_rows_filtered = [r for r in content_rows if r[1] in projectIds]
    ks = []
    m = None
    for k in projects.keys():
        try:
            projectInfos = list(
                filter(
                    lambda x: x[1] == k,
                    content_rows_filtered
                )
            )
            active_unit_sum = sum([int(p[8]) for p in projectInfos if p[8].isnumeric()])

            projectInfo = projectInfos[0]

            regionKey = -1
            project_region = projectInfo[5]
            project_district = projectInfo[7]
            project_subdistrict = projectInfo[6]
            print(project_region, project_district, project_subdistrict)

            province_filter = [r for r in regions if r.provinceNameEN == project_region]
            if len(province_filter) == 0:
                logging.info('No region found')
                regionKey = -1
            else:
                district_filter = [d for d in province_filter if d.districtNameEN == project_district]
                if len(district_filter) > 0:
                    logging.info('District found')
                    subdistrict_filter = [s for s in district_filter if s.subdistrictNameEN == project_subdistrict]
                    
                    if len(subdistrict_filter) > 0:
                        regionKey = subdistrict_filter[0].regionKey
                    else:
                        district_filter_fallback = [d for d in district_filter if d.subdistrictNameEN == None]
                        regionKey = district_filter_fallback[0].regionKey
                else:
                    logging.info('No district found found')
                    df = [d for d in district_filter if d.districtNameEN == None and d.subdistrictNameEN == None]
                    regionKey = df[0].regionKey

            projects[k]['RegionKey'] = regionKey
            projects[k]['ActiveUnits'] = active_unit_sum

            logging.info(f'Region assigned to {regionKey}')

            if 'Done' == projects[k]['ProjectStatus']:
                projects[k]['EndDate'] = datetime.datetime.today().date()

        except Exception as e:
            ks.append(k)
            print(e)
            print('=' * 20)

    for i in ks:
        del projects[i]

    return projects

def import_date_split(d):
    d = d.split(' ')[0].split('/')
    date_key = 99991231
    try: 
        date_key = int(d[2] + d[1] + d[0])
    except:
        date_key = 99991231

    return date_key

def status_date(status):
    return status in ('Active', 'Draft', 'In Progress')

def number_convert(n, default=0):
    n = n.replace('฿', '').replace(',', '')
    return float(n) if n.isnumeric() else default

def rental_price_name(rental_rate = '0'):
    rental_rate = number_convert(rental_rate, default=-1)
    rental_name = 'N/A'

    if -1 == rental_rate:
        rental_name = 'N/A'
    elif rental_rate > 50_000:
        rental_name = '50k+'
    elif rental_rate > 20_000:
        rental_name = '20k-50k+'
    elif rental_rate > 10_000:
        rental_name = '10k-20k+'
    else:
        rental_name = '<10k'
    
    return rental_name

def sale_price_name(sale_price):
    sale_price = number_convert(sale_price, default=-1)
    sale_name = 'N/A'

    if -1 == sale_price:
        sale_name = 'N/A'
    elif sale_price > 10_000_000:
        sale_name = '10m+'
    elif sale_price > 5_000_000:
        sale_name = '5-10m'
    elif sale_price > 2_000_000:
        sale_name = '2-5m'
    else:
        sale_name = '<2m'
    
    return sale_name

def indoor_area_convert(indoor_area):
    return float(indoor_area) if indoor_area.isnumeric() else 0

def unit_filter(unit_sheet, employee, project):
    active_units = [u for u in unit_sheet if '-' != u[25] and '-' != u[26]]
    units = []

    for u in active_units:
        unit_status = u[27]
        end_date = datetime.datetime(9999, 12, 31).date() if status_date(u[27]) else datetime.datetime.today().date()
        try:
            proj_id, region_id = project.get(int(u[25]))
        except Exception as e:
            logging.info(f'No data for project Id {u[25]}')
            continue

        units.append({
            'projectId': u[25],
            'unitIdAlternateKey': u[26],
            'unitStatus': u[27],
            'propertyType': u[28],
            'bedRooms': number_convert(u[38], default=0),
            'bathRooms': number_convert(u[39], default=0),
            'indoorArea': number_convert(u[40], default=0),
            'floor': u[41],
            'dealType': u[30],
            'ownership': u[52],
            'forSale': u[31],
            'unitPrice': number_convert(u[32]),
            'salePricename': sale_price_name(u[32]),
            'forRent': u[33],
            'monthlyRentalRate': number_convert(u[34]),
            'rentalRatePriceName': rental_price_name(u[34]),
            'regionKey': region_id,
            'dateKey': import_date_split(u[21]),
            'employeeKey': employee.get(u[28], -1),
            'projectKey': proj_id,
            'startDate': datetime.datetime.strptime(u[0], '%Y-%m-%d'),
            'endDate': end_date,
            'previousListingId': -1,
        })

    return units

def compare_listing_info(listing_unit, sheet_listing):
    return listing_unit.unitStatus == sheet_listing.get('unitStatus') \
        and listing_unit.propertyType == sheet_listing.get('propertyType') \
        and listing_unit.bedRooms == sheet_listing.get('bedRooms') \
        and listing_unit.bathRooms == sheet_listing.get('bathRooms') \
        and listing_unit.indoorArea == sheet_listing.get('indoorArea') \
        and listing_unit.floor == sheet_listing.get('floor') \
        and listing_unit.dealType == sheet_listing.get('dealType') \
        and listing_unit.ownership == sheet_listing.get('ownership') \
        and listing_unit.forSale == sheet_listing.get('forSale') \
        and listing_unit.unitPrice == sheet_listing.get('unitPrice') \
        and listing_unit.salePriceName == sheet_listing.get('salePriceName') \
        and listing_unit.forRent == sheet_listing.get('forRent') \
        and listing_unit.monthlyRentalRate == sheet_listing.get('monthlyRentalRate') \
        and listing_unit.rentalRatePriceName == sheet_listing.get('rentalRatePriceName') \
        and listing_unit.regionKey == sheet_listing.get('regionKey') \
        and listing_unit.dateKey == sheet_listing.get('dateKey') \
        and listing_unit.employeeKey == sheet_listing.get('employeeKey') \
        and listing_unit.projectKey == sheet_listing.get('projectKey') \
        and listing_unit.startDate == sheet_listing.get('startDate') \
        and listing_unit.endDate == sheet_listing.get('endDate') \
        and listing_unit.previousListingId == sheet_listing.get('previousListingId')

def create_fact_listing(new_sheet_update_unit, previous_id=-1):
    return FactListing(
        unitIdAlternateKey=new_sheet_update_unit.get('unitIdAlternateKey'),
        unitStatus=new_sheet_update_unit.get('unitStatus'),
        propertyType=new_sheet_update_unit.get('propertyType'),
        bedRooms=new_sheet_update_unit.get('bedRooms'),
        bathRooms=new_sheet_update_unit.get('bathRooms'),
        indoorArea=new_sheet_update_unit.get('indoorArea'),
        floor=new_sheet_update_unit.get('floor'),
        dealType=new_sheet_update_unit.get('dealType'),
        ownership=new_sheet_update_unit.get('ownership'),
        forSale=new_sheet_update_unit.get('forSale'),
        unitPrice=new_sheet_update_unit.get('unitPrice'),
        salePriceName=new_sheet_update_unit.get('salePriceName'),
        forRent=new_sheet_update_unit.get('forRent'),
        monthlyRentalRate=new_sheet_update_unit.get('monthlyRentalRate'),
        rentalRatePriceName=new_sheet_update_unit.get('rentalRatePriceName'),
        regionKey=new_sheet_update_unit.get('regionKey'),
        dateKey=new_sheet_update_unit.get('dateKey'),
        employeeKey=new_sheet_update_unit.get('employeeKey'),
        projectKey=new_sheet_update_unit.get('projectKey'),
        startDate=new_sheet_update_unit.get('startDate'),
        endDate=new_sheet_update_unit.get('endDate'),
        previousListingId=previous_id,
        isCurrentState=True
    )

def compact_listing(listing_unit, new_sheet_update_unit):
    listing_unit.isCurrentState = False
    listing_unit.endDate = datetime.datetime.today().date()

    new_listing = create_fact_listing(
        new_sheet_update_unit,
        listing_unit.listingKey
    )

    return listing_unit, new_listing

def merge_listing_info(listing):
    unitIds = [a.get('unitIdAlternateKey') for a in listing]
    listing_set = session.query(FactListing).filter(
        FactListing.unitIdAlternateKey.in_(unitIds)
    ).all()
    listing_set = set([l for l in listing_set if l.isCurrentState])

    update_listing = []
    logging.info("Update listing")
    for listing_unit in listing_set:
        new_sheet_update_unit = [a for a in listing if listing_unit.unitIdAlternateKey == a.get('unitIdAlternateKey')][0]
        update_listing.append(listing_unit.unitIdAlternateKey)
        if not compare_listing_info(listing_unit=listing_unit, sheet_listing=new_sheet_update_unit):
            logging.info('Update new')
            listing_unit, new_listing_unit = compact_listing(listing_unit, new_sheet_update_unit)
            
            session.add_all([
                listing_unit,
                new_listing_unit
            ])

        else:
            logging.info('No update')

    session.commit()

    # Add new listing
    logging.info("Insert new")
    for l in [k for k in listing if k.get('unitIdAlternateKey') not in update_listing]:
        session.add(
            create_fact_listing(l)
        )

    session.commit()

def project_compare(existing_project: dict, sheet_project: dict):
    return existing_project.get('ProjectAlternateKey', None) == int(sheet_project.get('ProjectAlternateKey', -1)) \
        and existing_project.get('ProjectName', None) == sheet_project.get('ProjectName', -1) \
        and existing_project.get('ActiveUnits', None) == int(sheet_project.get('ActiveUnits', -1)) \
        and existing_project.get('Assignee', None) == sheet_project.get('Assignee', -1)

def sync_all_projects(content=None, monthly_target=None, employee=None):
    if None == monthly_target or None == content or None == employee:
        logging.info('Empty info')
        return

    projects = project_filter(content_rows=content, monthly_target_rows=monthly_target)

    projectIds = [int(p[1:]) for p in projects.keys()]
    projectSet = session.query(DimProject).filter(
        DimProject.projectAlternateKey.in_(projectIds)
    ).all()

    today = datetime.datetime.today().date()
    logging.info('Starting update data existing project')
    for project in projectSet:
        p = projects.get(f'P{project.projectAlternateKey}', None)
        if None == p:
            logging.info(f'Project was deleted: {project.projectAlternateKey}')
            pid = project.projectKey
            newProject = DimProject(
                projectAlternateKey=project.projectAlternateKey,
                projectName=project.projectName,
                regionKey=project.regionKey,
                activeUnits=project.activeUnits,
                assignee=employee.get(project.assignee, -1),
                startDate=project.startDate,
                endDate=project.endDate,
                createdAt=today,
                endAt=datetime.datetime(9999, 12, 31).date(),
                isCurrent=True,
            )

            project.endAt = newProject.endAt
            project.isCurrent = False

            session.add_all([
                project,
                newProject
            ])
        else:
            logging.info('Column exists')
            if not project_compare(project.pack(), p):
                logging.info(f'Add new version for {p.get("ProjectAlternateKey")}@{project.projectKey}')
                new_project_info = DimProject(
                    projectAlternateKey=p.get('ProjectAlternateKey'),
                    projectName=p.get('ProjectName'),
                    regionKey=p.get('RegionKey'),
                    activeUnits=p.get('ActiveUnits'),
                    assignee=employee.get(p.get('Assignee'), -1),
                    startDate=p.get('StartDate'),
                    endDate=p.get('EndDate'),
                    createdAt=today,
                    endAt=datetime.datetime(9999, 12, 31).date(),
                    isCurrent=True,
                )

                project.isCurrent = False
                project.endAt = today 

                session.add_all([
                    project,
                    new_project_info
                ])
            else:
                logging.info(f'No data update for {p.get("ProjectAlternateKey")}')

            del projects[f'P{p.get("ProjectAlternateKey")}']

    session.commit()

    if len(projects) > 0:
        logging.info('Adding new items')

    new_projects = []
    for key, project in projects.items():
        new_projects.append({
            'ProjectAlternateKey': project.get('ProjectAlternateKey'),
            'ProjectName': project.get('ProjectName'),
            'RegionKey': project.get('RegionKey'),
            'ActiveUnits': project.get('ActiveUnits'),
            'Assignee': employee.get(project.get('Assignee'), -1),
            'StartDate': project.get('StartDate'),
            'EndDate': project.get('EndDate'),
            'CreatedAt': today,
            'EndAt': datetime.datetime(9999, 12, 31).date(),
            'IsCurrent': True,
        })

    if len(new_projects) > 0:
        pg_hook = PostgresHook(
            postgres_conn_id='dw_connection',
            schema='airflow'
        )
        engine = pg_hook.get_sqlalchemy_engine()
        engine.execute(
            DimProject.__table__.insert(),
            new_projects
        )

    return []

def merge_projects(sheet_projects, existing_projects):
    return []

def update_all_projects():
    employee = {e.employeeIdAlternateKey: e.employeeKey for e in session.query(DimEmployee).all()}

    projects = []
    content_sheet = get_content_worksheet()
    monthly_target_sheet = get_monthly_target_worksheet()

    sync_all_projects(
        content=content_sheet,
        monthly_target=monthly_target_sheet,
        employee=employee
    )

    return []

def update_all_employee(task_instance):
    content_sheet = get_content_worksheet()
    content_unit_sheet = get_employee()
    monthly_target_sheet = get_monthly_target_worksheet()
    monthly_matterport = get_monthly_target_matterport()

    employee = employee_filter(
        content_employee=content_sheet,
        content_unit_sheet=content_unit_sheet,
        monthly_main_sheet=monthly_target_sheet,
        monthly_matterport=monthly_matterport
    )


    employeeSet = session.query(DimEmployee).filter(
        DimEmployee.employeeIdAlternateKey.in_(employee)
    ).all()

    existing_employee = set([e.employeeIdAlternateKey for e in employeeSet])

    logging.info('Remove for missing employee')
    for e in existing_employee - employee:
        logging.info('Remove employee', e)
        emp = session.query(DimEmployee).filter(
            employeeIdAlternateKey=e.employeeIdAlternateKey
        ).all()

        emp.endDate = datetime.datetime.today().date()
        session.add(emp)
    session.commit()

    logging.info('Add new employee')
    for e in employee - existing_employee:
        logging.info('Employee', e)
        session.add(DimEmployee(
            employeeIdAlternateKey=e,
            startDate=datetime.datetime.today().date(),
            endDate=datetime.datetime(9999, 12, 31).date()
        ))
    session.commit()

    return {e.employeeIdAlternateKey: e.employeeKey for e in session.query(DimEmployee).all()}

def update_all_units():
    pq = session.query(DimProject).all()
    projects = {p.projectAlternateKey: [p.projectKey, p.regionKey] for p in pq if p.isCurrent}
    employee = {e.employeeIdAlternateKey: e.employeeKey for e in session.query(DimEmployee).all()}
    regions_info = get_all_regions()

    active_units = unit_filter(
        unit_sheet=get_monthly_target_worksheet(),
        employee=employee,
        project=projects
    )

    merge_listing_info(listing=active_units)