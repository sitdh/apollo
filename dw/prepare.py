import io
import logging

from csv import DictReader
from airflow.hooks.postgres_hook import PostgresHook
from airflow.models import Variable

def init_tables(ti):
    status_indicator = False
    if not Variable.get('is_db_init', False):
        from dw.models import init_db
        status_indicator = init_db()
    
    Variable.set('is_table_created', True)
    ti.xcom_push(key='table_created', value=status_indicator)

def retrieve_regions_source(ti):
    import requests
    logging.info('Getting data from source')
    regions_location = Variable.get('apollo_regions_info')

    response = ''
    try:
        response = str(requests.get(regions_location).text)
    except:
        response = ''

    ti.xcom_push(key='geolocation_resource', value=response)

def ingest_geograph_info(ti):
    from dw.models import DimRegion

    response = ti.xcom_pull(key='geolocation_resource')
    if None == response:
        logging.info('No data to be update')
        return

    remove_asterisk = lambda x: x.replace('*', '')
    remove_prefix = lambda x: x[3:] if 'เขต' == x[:3] else x
    remove_prefix_ii = lambda x: x[5:] if 'Khet ' == x[:5] else x
    clean_text = lambda x: remove_prefix_ii(remove_prefix(remove_asterisk(x)))

    regions = DictReader(io.StringIO(response))
    process_status = False
    
    logging.info('Getting pre-define destination db connection')
    pg_hook = PostgresHook(
        postgres_conn_id='dw_connection',
        schema='airflow'
    )
    engine = pg_hook.get_sqlalchemy_engine()

    logging.info('Openning region file')
    regions_list = []
    for r in regions:
        regions_list.append({
            'RegionKey': r.get('district_code'),
            'ProvinceCode': r.get('province_code'),
            'ProvinceNameTH': clean_text(r.get('province_name_th')),
            'ProvinceNameEN': clean_text(r.get('province_name_en')),
            'DistrictCode': r.get('amphure_code'),
            'DistrictNameTH': clean_text(r.get('amphure_name_th')),
            'DistrictNameEN': clean_text(r.get('amphure_name_en')),
            'SubdistrictCode': r.get('district_code'),
            'SubdistrictNameTH': clean_text(r.get('district_name_th')),
            'SubdistrictNameEN': clean_text(r.get('district_name_en')),
        })
    
    if len(regions_list):
        logging.info('Starting insert regions info')
        engine.execute(
            DimRegion.__table__.insert(),
            regions_list
        )
        logging.info('All regions inserted')

    # Provinces only
    provinces = [] 
    provinces_index = []
    for r in regions_list:
        if not r.get('ProvinceCode') in provinces:
            provinces.append(r.get('ProvinceCode'))
            provinces_index.append({
                'RegionKey': int(r.get('ProvinceCode')) * 10000,
                'ProvinceCode': r.get('ProvinceCode'),
                'ProvinceNameTH': r.get('ProvinceNameTH'),
                'ProvinceNameEN': r.get('ProvinceNameEN'),
                'DistrictCode': None,
                'DistrictNameTH': None,
                'DistrictNameEN': None,
                'SubdistrictCode': None,
                'SubdistrictNameTH': None,
                'SubdistrictNameEN': None,
            })
    if len(provinces_index):
        logging.info('Start insert province only')
        engine.execute(
            DimRegion.__table__.insert(),
            provinces_index
        )
        logging.info('All provinces inserted')

    provinces = []
    provinces_index = []
    for r in regions_list:
        if not r.get('DistrictCode') in provinces:
            provinces.append(r.get('DistrictCode'))
            provinces_index.append({
                'RegionKey': int(r.get('DistrictCode')) * 100,
                'ProvinceCode': r.get('ProvinceCode'),
                'ProvinceNameTH': r.get('ProvinceNameTH'),
                'ProvinceNameEN': r.get('ProvinceNameEN'),
                'DistrictCode': r.get('DistrictCode'),
                'DistrictNameTH': r.get('DistrictNameTH'),
                'DistrictNameEN': r.get('DistrictNameEN'),
                'SubdistrictCode': None,
                'SubdistrictNameTH': None,
                'SubdistrictNameEN': None,
            })

    if len(provinces_index):
        logging.info('Start insert province only')
        engine.execute(
            DimRegion.__table__.insert(),
            provinces_index
        )
        logging.info('All provinces inserted')

    process_status = len(regions_list) > 0

    Variable.set('is_table_created', True)