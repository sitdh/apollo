import json
import logging

from typing import Optional
from datetime import timedelta, datetime, date
from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.python import PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.dates import days_ago
from airflow.utils.task_group import TaskGroup
from airflow.models import Variable, DAG

from dw.prepare import ingest_geograph_info, retrieve_regions_source, init_tables
from dw.sheets import update_all_projects, update_all_employee, update_all_units

# These args will get passed on to each operator
# You can override them on a per-task basis during operator initialization
default_args = {
    'owner': 'sitdhibong',
    'email': ['sitdhibong@gmail.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

logger = logging.getLogger(__name__)

with DAG('Apollo', default_args=default_args, schedule_interval="@once", start_date=days_ago(1), tags=['apollo', 'dw', 'etl']) as dag:
    def prepare_for_dates(task_instance):
        from dw.dategen import date_calculator, insert_dates

        DAYS_IN_A_YEAR = 365
        start_date = Variable.get('prepare_future_datetime', None)

        date_range = []
        if None == start_date or 'date' != type(start_date).__name__:
            date_range = [date.today() - timedelta(days=d) for d in range(DAYS_IN_A_YEAR * 2, 0, -1)]
            date_range += [date.today() + timedelta(days=d) for d in range(0, DAYS_IN_A_YEAR + 1)]
        else:
            start_date = datetime.strptime('%Y-%m-%d', start_date).date + timedelta(days=1)
            date_range = [start_date + timedelta(days=d) for d in range(DAYS_IN_A_YEAR + 1)]

        insert_dates(
            entities=date_calculator(date_range=date_range)
        )

        end_date = date_range[-1] - timedelta(days=30)
        Variable.set('prepare_future_datetime', end_date)

        return True

    def retrieved_google_sheets(task_instance):
        
        return True

    def define_future_date():
        predefine_date = datetime.today().date()
        try:
            predefine_date = Variable.get('prepare_future_datetime')
            predefine_date = datetime.strptime(predefine_date, '%Y-%m-%d').date()
        except:
            pass

        return 'predefine_datetime' if predefine_date <= date.today() else 'sync_googlesheet'

    def sync_project_from_sheet():
        update_all_projects()

        return []

    def sync_google_sheet_listing():
        update_all_units()

        return []

    with TaskGroup(group_id='tables_initiation', tooltip='Tables initiation for first time execution') as section_preparation:

        init_tables_operator = PythonOperator(
            task_id='init_tables_operator',
            python_callable=init_tables,
            provide_context=True,
        )

        retrieve_geograph_source = PythonOperator(
            task_id='retrieve_geograph_source',
            python_callable=retrieve_regions_source,
            provide_context=True,
        )

        prepare_geograph_info = PythonOperator(
            task_id='prepare_geograph_info',
            python_callable=ingest_geograph_info,
            provide_context=True,
        )

        init_tables_operator >> retrieve_geograph_source >> prepare_geograph_info

    predefine_datetime = PythonOperator(
        task_id='predefine_datetime',
        python_callable=prepare_for_dates
    )

    sync_googlesheet = PythonOperator(
        task_id='sync_googlesheet',
        python_callable=retrieved_google_sheets,
        trigger_rule='none_failed',
    )

    is_table_init = BranchPythonOperator(
        task_id='is_table_init',
        python_callable=lambda: 'is_date_should_prepare' if Variable.get('is_table_created', False) else 'tables_initiation.init_tables_operator',
    )

    is_date_should_prepare = BranchPythonOperator(
        task_id='is_date_should_prepare',
        python_callable=define_future_date,
        trigger_rule='none_failed',
    )

    launcher = DummyOperator(
        task_id='task_launcher',
    )

    sync_employees = PythonOperator(
        task_id='sync_employees',
        python_callable=update_all_employee
    )

    google_sheet_sync_project = PythonOperator(
        task_id='google_sheet_sync_group',
        python_callable=sync_project_from_sheet
    )

    google_sheet_update_listing = BranchPythonOperator(
        task_id='google_sheet_update_listing',
        python_callable=sync_google_sheet_listing
    )


    launcher >> is_table_init >> [section_preparation, is_date_should_prepare]
    section_preparation >> is_date_should_prepare >> [predefine_datetime, sync_googlesheet]
    predefine_datetime >> sync_googlesheet >> sync_employees >> google_sheet_sync_project >> google_sheet_update_listing

    # launcher >> is_table_init >> [section_preparation, is_date_should_prepare]
    # section_preparation >> is_date_should_prepare >> [date_preparation, merging]
    # date_preparation >> merging